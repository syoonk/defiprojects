pragma solidity ^0.7.3;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/math/SafeMath.sol";

contract CollateralBackedToken is ERC20 {

  using SafeMath for uint;

  IERC20 public collateral;
  uint public price = 1;

  constructor(address _collateral) 
  ERC20('Collateral Backed Token', 'CBT') {
    collateral = IERC20(_collateral);
  }

  function deposit(uint _collateralAmount)
  external {
    collateral.transferFrom(msg.sender, address(this), _collateralAmount);
    _mint(msg.sender, _collateralAmount.mul(price));
  }

  function withdraw(uint _tokenAmount)
  external {
    require(balanceOf(msg.sender) >= _tokenAmount,
      "balance is too low");
    
    _burn(msg.sender, _tokenAmount);
    collateral.transfer(msg.sender, _tokenAmount.div(price));
  }
}
const CoinGecko = require('coingecko-api');
const Oracle = artifacts.require('Oracle.sol');

const POLL_INTERVAL = 5000;
const CoinGeckoClient = new CoinGecko();

module.exports = async done => {
  const [_, reporter] = await web3.eth.getAccounts();
  const oracle = await Oracle.deployed();
  
  while(true) {
    try {
      const response = await CoinGeckoClient.coins.fetch('bitcoin', {});

      let currentPrice = parseFloat(response.data.market_data.current_price.usd);
      currentPrice = parseInt(currentPrice * 100);

      const receipt = await oracle.updateData(
        web3.utils.soliditySha3('BTC/USD'),
        currentPrice,
        {from: reporter}
      );

      console.log(`new price for BTC/USD ${currentPrice} updated on-chain`);
    } catch(_err) {
      console.log(`updating price error: `, _err);
    }

    await new Promise(resolve => setTimeout(resolve, POLL_INTERVAL));
  }

  done();
}
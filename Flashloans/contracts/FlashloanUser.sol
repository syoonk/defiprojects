pragma solidity ^0.7.3;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "./FlashloanProvider.sol";
import "./IFlashloanUser.sol";

contract FlashloanUser is IFlashloanUser {

  function startFlashloan(
    address _flashloan,
    uint _amount,
    address _token
  ) external {
    FlashloanProvider(_flashloan).executeFlashloan(
      address(this),
      _amount,
      _token,
      bytes('')
    );
  }
  
  function flashloanCallback(
    uint _amount,
    address _token,
    bytes memory _data
  ) override external {
    // do some arbitrage, liquidation, etc ...

    // reimburse borrowed tokens
    IERC20(_token).transfer(msg.sender, _amount);
  }

}